import asyncio
import grpc
import ping_pb2
import ping_pb2_grpc
import time
import logging
import os

logging.basicConfig(level=logging.INFO)

async def start_ping_pong():
    logging.info("Starting start_ping_pong")
    pong_count = 0
    #channel = grpc.aio.insecure_channel('localhost:5555')
    server_url =  os.environ.get('GRPC_SERVER_URL')

    if server_url:
        channel = grpc.aio.secure_channel(server_url, grpc.ssl_channel_credentials())
    else:
        channel = grpc.aio.insecure_channel('localhost:5555')

    stub = ping_pb2_grpc.PingStub(channel)
    stream = stub.PingPong()

    while True:
        await asyncio.sleep(2)
        await stream.write(ping_pb2.PingPing(time=int(time.time()), pongCount=pong_count))
        pong = await anext(stream.__aiter__())
        pong_count += 1
        logging.info("Received pong at time %s with pingCount %d", pong.time, pong.pingCount)

async def main():
    await start_ping_pong()

if __name__ == "__main__":
    asyncio.run(main())
